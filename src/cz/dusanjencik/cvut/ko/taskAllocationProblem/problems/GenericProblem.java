package cz.dusanjencik.cvut.ko.taskAllocationProblem.problems;

import cz.dusanjencik.cvut.ko.taskAllocationProblem.problems.data.Facility;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.problems.data.Task;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com 
 * created 04.05.16
 *
 * Class for creating a problem from arrays of Facilites and Tasks.
 */
public class GenericProblem extends AProblem {
	public static final String TAG = "GenericProblem";

	private final Facility[] facilities;
	private final Task[]     tasks;

	public GenericProblem(Facility[] facilities, Task[] tasks) {
		super(true);
		this.facilities = facilities;
		this.tasks = tasks;
		init();
	}

	@Override
	protected int initNumOfFacilities() {
		return facilities.length;
	}

	@Override
	protected int initNumOfTasks() {
		return tasks.length;
	}

	@Override
	protected int[] initReleaseTime() {
		int[] releaseTimes = new int[tasks.length];
		for (int i = 0; i < tasks.length; ++i) {
			releaseTimes[i] = tasks[i].releaseTime;
		}
		return releaseTimes;
	}

	@Override
	protected int[] initDeadline() {
		int[] deadlines = new int[tasks.length];
		for (int i = 0; i < tasks.length; ++i) {
			deadlines[i] = tasks[i].deadline;
		}
		return deadlines;
	}

	@Override
	protected double[] initFacilityCapacity() {
		double[] capacities = new double[facilities.length];
		for (int i = 0; i < facilities.length; ++i) {
			capacities[i] = facilities[i].capacity;
		}
		return capacities;
	}

	@Override
	protected int[][] initProcessingTimePerFacility() {
		int[][] processingTimes = new int[facilities.length][tasks.length];
		for (int i = 0; i < facilities.length; ++i) {
			for (int j = 0; j < tasks.length; ++j) {
				processingTimes[i][j] = tasks[j].processingTimePerFacility[i];
			}
		}
		return processingTimes;
	}

	@Override
	protected double[][] initCapacityPerFacility() {
		double[][] capacities = new double[facilities.length][tasks.length];
		for (int i = 0; i < facilities.length; ++i) {
			for (int j = 0; j < tasks.length; ++j) {
				capacities[i][j] = tasks[j].capacityPerFacility[i];
			}
		}
		return capacities;
	}

	@Override
	protected double[][] initFixedCostPerFacility() {
		double[][] costs = new double[facilities.length][tasks.length];
		for (int i = 0; i < facilities.length; ++i) {
			for (int j = 0; j < tasks.length; ++j) {
				costs[i][j] = tasks[j].fixedCostPerFacility[i];
			}
		}
		return costs;
	}
}
