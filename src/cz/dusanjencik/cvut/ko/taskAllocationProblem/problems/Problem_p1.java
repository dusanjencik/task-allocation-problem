package cz.dusanjencik.cvut.ko.taskAllocationProblem.problems;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 01.05.16
 */
public class Problem_p1 extends AProblem {
	public static final String TAG = "Problem_p1";

	public Problem_p1() {
		super(false);
	}

	@Override
	protected int initNumOfFacilities() {
		return 1;
	}

	@Override
	protected int initNumOfTasks() {
		return 3;
	}

	@Override
	protected int[] initReleaseTime() {
		return new int[]{0, 0, 0};
	}

	@Override
	protected int[] initDeadline() {
		return new int[]{4, 4, 4};
	}

	@Override
	protected double[] initFacilityCapacity() {
		return new double[]{100};
	}

	@Override
	protected int[][] initProcessingTimePerFacility() {
		return new int[][]{
				{3, 2, 2} // facility 1
		};
	}

	@Override
	protected double[][] initCapacityPerFacility() {
		return new double[][]{
				{20, 35, 70} // facility 1
		};
	}

	@Override
	protected double[][] initFixedCostPerFacility() {
		return new double[][]{
				{1, 1, 1} // facility 1
		};
	}
}
