package cz.dusanjencik.cvut.ko.taskAllocationProblem.problems;

import cz.dusanjencik.cvut.ko.taskAllocationProblem.problems.data.Facility;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.problems.data.Task;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 01.05.16
 *
 * Problem from given paper.
 */
public class Problem_2facilities_8tasks extends GenericProblem {
	public static final String TAG = "Problem_2facilities_8tasks";

	private static final int facilityCount = 2;

	public Problem_2facilities_8tasks() {
		super(new Facility[]{
				// capacity
				new Facility(100),
				new Facility(80)
		}, new Task[]{
				// releaseTime, processingTime, deadline, numberOfFacilities, capacityPerAllFacilities, fixedCostPerAllFacilites
				new Task(0, 4, 4, facilityCount, 100, 1), // T1
				new Task(4, 6, 10, facilityCount, 70, 1), // T2
				new Task(4, 3, 7, facilityCount, 5, 1), // T3
				new Task(7, 1, 8, facilityCount, 30, 1), // T4
				new Task(10, 1, 11, facilityCount, 80, 1), // T5
				new Task(0, 5, 5, facilityCount, 60, 1), // T6
				new Task(3, 3, 6, facilityCount, 20, 1), // T7
				new Task(6, 6, 12, facilityCount, 70, 1), // T8
		});
	}
}
