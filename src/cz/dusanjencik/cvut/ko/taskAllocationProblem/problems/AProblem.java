package cz.dusanjencik.cvut.ko.taskAllocationProblem.problems;

import java.io.Serializable;
import java.util.Arrays;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 01.05.16
 *
 * Abstract class for problem instances.
 */
public abstract class AProblem implements Serializable {
	public static final String TAG = "AProblem";

	private int
			numOfFacilities, // i
			numOfTasks, // j
			numOfTimes; // t
	private int[]
			releaseTime, // r_j
			deadline; // d_j
	private double[]
			facilityCapacity;// C_i
	private int[][]
			processingTimePerFacility; // p_{i, j}
	private double[][]
			capacityPerFacility, // c_{i, j}
			fixedCostPerFacility; // F_{i, j}

	/**
	 * @param lazyInit If it is true, init() must be called necessary explicitly.
	 */
	public AProblem(boolean lazyInit) {
		if (!lazyInit) init();
	}

	@SuppressWarnings("OptionalGetWithoutIsPresent")
	protected void init() {
		this.numOfFacilities = initNumOfFacilities();
		this.numOfTasks = initNumOfTasks();
		this.releaseTime = initReleaseTime();
		this.deadline = initDeadline();
		this.facilityCapacity = initFacilityCapacity();
		this.processingTimePerFacility = initProcessingTimePerFacility();
		this.capacityPerFacility = initCapacityPerFacility();
		this.fixedCostPerFacility = initFixedCostPerFacility();
		this.numOfTimes = Arrays.stream(deadline).max().getAsInt();
	}

	protected abstract int initNumOfFacilities();

	protected abstract int initNumOfTasks();

	protected abstract int[] initReleaseTime();

	protected abstract int[] initDeadline();

	protected abstract double[] initFacilityCapacity();

	protected abstract int[][] initProcessingTimePerFacility();

	protected abstract double[][] initCapacityPerFacility();

	protected abstract double[][] initFixedCostPerFacility();

	public int getNumOfFacilities() {
		return numOfFacilities;
	}

	public int getNumOfTasks() {
		return numOfTasks;
	}

	public int getNumOfTimes() {
		return numOfTimes;
	}

	public int[] getReleaseTimes() {
		return releaseTime;
	}

	public int[] getDeadlines() {
		return deadline;
	}

	public double[] getFacilityCapacities() {
		return facilityCapacity;
	}

	public int[][] getProcessingTimePerFacility() {
		return processingTimePerFacility;
	}

	public double[][] getCapacityPerFacility() {
		return capacityPerFacility;
	}

	public double[][] getFixedCostPerFacility() {
		return fixedCostPerFacility;
	}

	@Override
	public String toString() {
		return "AProblem{\n" +
				"numOfFacilities=" + numOfFacilities +
				",\n numOfTasks=" + numOfTasks +
				",\n numOfTimes=" + numOfTimes +
				",\n releaseTime=" + Arrays.toString(releaseTime) +
				",\n deadline=" + Arrays.toString(deadline) +
				",\n facilityCapacity=" + Arrays.toString(facilityCapacity) +
				",\n processingTimePerFacility=" + Arrays.deepToString(processingTimePerFacility) +
				",\n capacityPerFacility=" + Arrays.deepToString(capacityPerFacility) +
				",\n fixedCostPerFacility=" + Arrays.deepToString(fixedCostPerFacility) +
				"\n}";
	}
}
