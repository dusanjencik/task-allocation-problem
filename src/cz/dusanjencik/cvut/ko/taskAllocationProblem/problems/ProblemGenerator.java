package cz.dusanjencik.cvut.ko.taskAllocationProblem.problems;

import cz.dusanjencik.cvut.ko.taskAllocationProblem.problems.data.Facility;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.problems.data.Task;

import java.util.Random;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com 
 * created 04.05.16
 *
 * Class for generating problems defined by number of tasks and number of facilities.
 * C_i = 10
 * c = random[1, 10]
 * p = random[i, 10*i]
 * r = 0
 * d = max((alpha * 10 / 2 * numberOfTasks * (numberOfFacilities + 1) / numberOfFacilities, r + p)
 * F = random[2 * (numberOfFacilities - i + 1), 2 * 10 * (numberOfFacilities - i + 1)]
 */
public class ProblemGenerator {
	public static final String TAG = "ProblemGenerator";

	private static final double ALPHA             = 1 / 3.;
	private static final int    CAPACITY_FACILITY = 10;

	private Random r = new Random();

	private int numberOfFacilities, numberOfTasks;

	public ProblemGenerator(int numberOfFacilities, int numberOfTasks) {
		this.numberOfFacilities = numberOfFacilities;
		this.numberOfTasks = numberOfTasks;
		this.r = new Random();
	}

	private int nextRandom(int min, int max) {
		return min + r.nextInt(max - min + 1);
	}

	public AProblem generateProblem() {
		Facility[] facilities = new Facility[numberOfFacilities];
		Task[]     tasks      = new Task[numberOfTasks];
		for (int i = 0; i < numberOfFacilities; ++i)
			facilities[i] = new Facility(CAPACITY_FACILITY);
		for (int j = 0; j < numberOfTasks; ++j) {
			for (int i = 0; i < numberOfFacilities; ++i) {
				int c = nextRandom(1, CAPACITY_FACILITY);
				int p = nextRandom(i, CAPACITY_FACILITY * i);
				int r = 0;
				int d = (int) Math.round(ALPHA * CAPACITY_FACILITY / 2 * numberOfTasks * (numberOfFacilities + 1) / numberOfFacilities);
				d = Math.max(d, r + p);
				int F = nextRandom(2 * (numberOfFacilities - i + 1), 2 * CAPACITY_FACILITY * (numberOfFacilities - i + 1));
				tasks[j] = new Task(r, p, d, numberOfFacilities, c, F);
			}
		}
		return new GenericProblem(facilities, tasks);
	}


}
