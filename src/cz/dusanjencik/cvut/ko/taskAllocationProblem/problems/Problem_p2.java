package cz.dusanjencik.cvut.ko.taskAllocationProblem.problems;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 01.05.16
 */
public class Problem_p2 extends AProblem {
	public static final String TAG = "Problem_p2";

	public Problem_p2() {
		super(false);
	}

	@Override
	protected int initNumOfFacilities() {
		return 1;
	}

	@Override
	protected int initNumOfTasks() {
		return 4;
	}

	@Override
	protected int[] initReleaseTime() {
		return new int[]{4, 1, 1, 0};
	}

	@Override
	protected int[] initDeadline() {
		return new int[]{7, 5, 6, 4};
	}

	@Override
	protected double[] initFacilityCapacity() {
		return new double[]{1};
	}

	@Override
	protected int[][] initProcessingTimePerFacility() {
		return new int[][]{
				{2, 1, 2, 2} // facility 1
		};
	}

	@Override
	protected double[][] initCapacityPerFacility() {
		return new double[][]{
				{1, 1, 1, 1} // facility 1
		};
	}

	@Override
	protected double[][] initFixedCostPerFacility() {
		return new double[][]{
				{2, 1, 2, 2} // facility 1
		};
	}
}
