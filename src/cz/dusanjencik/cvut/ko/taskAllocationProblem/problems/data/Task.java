package cz.dusanjencik.cvut.ko.taskAllocationProblem.problems.data;

import java.io.Serializable;
import java.util.Arrays;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com 
 * created 03.05.16
 *
 * Data class for a task.
 */
public class Task implements Serializable{
	public static final String TAG = "Task";

	public int releaseTime, deadline;
	public int[] processingTimePerFacility, capacityPerFacility, fixedCostPerFacility;

	public Task(int releaseTime, int processingTimePerAllFacilities, int deadline, int numberOfFacilities,
				int capacityPerAllFacilities, int fixedCostPerAllFacilities) {
		if (deadline - releaseTime < processingTimePerAllFacilities)
			throw new IllegalArgumentException("Processing time must be shorter than given interval.");
		this.releaseTime = releaseTime;
		this.processingTimePerFacility = new int[numberOfFacilities];
		Arrays.fill(this.processingTimePerFacility, processingTimePerAllFacilities);
		this.deadline = deadline;
		this.capacityPerFacility = new int[numberOfFacilities];
		Arrays.fill(this.capacityPerFacility, capacityPerAllFacilities);
		this.fixedCostPerFacility = new int[numberOfFacilities];
		Arrays.fill(this.fixedCostPerFacility, fixedCostPerAllFacilities);
	}

	public Task setCapacityPerFacility(int capacityPerFacility) {
		Arrays.fill(this.capacityPerFacility, capacityPerFacility);
		return this;
	}

	public Task setCapacityPerFacility(int... capacityPerFacility) {
		this.capacityPerFacility = capacityPerFacility;
		return this;
	}

	public Task setFixedCostPerFacility(int fixedCostPerFacility) {
		Arrays.fill(this.fixedCostPerFacility, fixedCostPerFacility);
		return this;
	}

	public Task setFixedCostPerFacility(int... fixedCostPerFacility) {
		this.fixedCostPerFacility = fixedCostPerFacility;
		return this;
	}
}
