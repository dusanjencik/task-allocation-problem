package cz.dusanjencik.cvut.ko.taskAllocationProblem.problems.data;

import java.io.Serializable;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com 
 * created 03.05.16
 *
 * Data class for a facility.
 */
public class Facility implements Serializable{
	public static final String TAG = "Facility";

	public int capacity, speed;

	public Facility(int capacity) {
		this.capacity = capacity;
		this.speed = 1;
	}

	public Facility(int capacity, int speed) {
		this.capacity = capacity;
		this.speed = speed;
	}
}
