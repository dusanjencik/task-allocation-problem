package cz.dusanjencik.cvut.ko.taskAllocationProblem;

import cz.dusanjencik.cvut.ko.taskAllocationProblem.helpers.ProgressBar;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.helpers.VerboseLevel;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.problems.AProblem;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.problems.ProblemGenerator;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.solutions.Solution;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.solutions.TimeoutSolution;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.solvers.benders.BendersDecomposition;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.solvers.milp.MILP;
import ilog.concert.IloException;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com 
 * created 05.05.16
 *
 * Benchmarking problems for measuring compute time.
 * Every generated problem is run 12 times and the minimum and maximum values are deleted.
 * Other values are averaged.
 */
public class Benchmark {
	public static final String TAG = "Benchmark";

	private static final int ITERATIONS = 20;
	private static final int TIME_LIMIT = 15 * 60; // sec

	public static void main(String[] args) throws IloException {
		for (int tasks = 10; tasks <= 16; tasks += 2) {
			run(ITERATIONS, 2, tasks);
		}
	}

	private static void run(int iterations, int numberFacilities, int numberTasks) throws IloException {
		boolean cutMinMax = true;
		if (iterations <= 5) cutMinMax = false;
		if (cutMinMax)
			iterations += 2;
		System.out.println("\n\n\n\n===================================" +
								   "\n         BENCHMARK RESULTS" +
								   "\n===================================");
		System.out.println("Number of iterations = " + (cutMinMax ? (iterations - 2) : iterations));
		System.out.println("Number of facilities = " + numberFacilities);
		System.out.println("Number of tasks = " + numberTasks);


		double[][]   timing       = new double[2][iterations];
		ProgressBar  progressBar  = new ProgressBar();
		VerboseLevel verboseLevel = VerboseLevel.NONE;

		int indexMin1 = 0, indexMin2 = 0, indexMax1 = 0, indexMax2 = 0;
		double min1 = Double.MAX_VALUE,
				min2 = Double.MAX_VALUE,
				max1 = Double.MIN_VALUE,
				max2 = Double.MIN_VALUE;

		ProblemGenerator problemGenerator = new ProblemGenerator(numberFacilities, numberTasks);
		AProblem         problem;
		progressBar.updateProgress(0);
		for (int i = 0; i < iterations; ) {
			problem = problemGenerator.generateProblem();
			long     elapsedTimeMILP = System.nanoTime();
			Solution solutionMILP    = new MILP(problem, verboseLevel).setTimeLimit(TIME_LIMIT).solve();
			if (solutionMILP instanceof TimeoutSolution) System.err.println("MILP timeout");
			if (solutionMILP.isSolved() || solutionMILP instanceof TimeoutSolution) {
				elapsedTimeMILP = System.nanoTime() - elapsedTimeMILP;

				long startTimeBenders = System.nanoTime();
				Solution solutionBenders = new BendersDecomposition(problem, verboseLevel)
						.setTimeLimit(TIME_LIMIT).solve();

				// save only if both solutions are solved
				if (solutionBenders instanceof TimeoutSolution) System.err.println("LBBD timeout");
				if (solutionBenders.isSolved() || solutionBenders instanceof TimeoutSolution) {
					progressBar.updateProgress((int) (i * 100. / (iterations - 1)));
					timing[0][i] = elapsedTimeMILP;
					timing[1][i] = System.nanoTime() - startTimeBenders;

					// remember min/max values
					if (timing[0][i] < min1) {
						min1 = timing[0][i];
						indexMin1 = i;
					}
					if (timing[0][i] > max1) {
						max1 = timing[0][i];
						indexMax1 = i;
					}
					if (timing[1][i] < min2) {
						min2 = timing[1][i];
						indexMin2 = i;
					}
					if (timing[1][i] > max2) {
						max2 = timing[1][i];
						indexMax2 = i;
					}

					++i; // next iteration
				}
			}
		}


		// compute average without minimum and maximum
		if (!cutMinMax) {
			indexMin1 = indexMin2 = indexMax1 = indexMax2 = -1;
		}
		double avgMILP = 0, avgBD = 0;
		for (int i = 0; i < iterations; ++i) {
			if (i != indexMin1 && i != indexMax1)
				avgMILP += timing[0][i];
			if (i != indexMin2 && i != indexMax2)
				avgBD += timing[1][i];
		}
		if (cutMinMax) {
			avgMILP /= iterations - 2;
			avgBD /= iterations - 2;
		} else {
			avgMILP /= iterations;
			avgBD /= iterations;
		}

		System.out.printf("MILP: %.3f s\nBender's decomposition %.3f s\n",
						  avgMILP / 1000000000., avgBD / 1000000000.);
	}
}
