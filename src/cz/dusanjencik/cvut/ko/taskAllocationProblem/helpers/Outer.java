package cz.dusanjencik.cvut.ko.taskAllocationProblem.helpers;

import org.jetbrains.annotations.Contract;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com 
 * created 07.05.16
 *
 * Wrapper class for any type to simulate returning value T from method inserted as parameter.
 * Usage:
 * Outer<String> str = new Outer<String>("str1"); // "str1" can be null
 * System.out.println(str); // str1
 * testMethod(str);
 * System.out.println(str); // str2
 * void testMethod(Outer<String> out) { out.set("str2"); }
 */
public final class Outer<T> {
	public static final String TAG = "Outer<T>";

	private T ref;

	public Outer(T ref) {
		this.ref = ref;
	}

	@Contract(pure = true)
	public T get() {
		return ref;
	}

	public T set(T newInstance) {
		ref = newInstance;
		return ref;
	}

	@Override
	public String toString() {
		return ref.toString();
	}
}

