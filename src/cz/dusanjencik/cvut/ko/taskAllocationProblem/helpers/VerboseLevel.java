package cz.dusanjencik.cvut.ko.taskAllocationProblem.helpers;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com 
 * created 04.05.16
 *
 * Level of detailed printed log.
 */
public enum VerboseLevel {
	NONE(0),
	BASIC(1),
	DETAILED(2);

	private final int level;

	VerboseLevel(int level) {
		this.level = level;
	}

	public boolean atLeast(VerboseLevel other) {
		return level >= other.level;
	}
}
