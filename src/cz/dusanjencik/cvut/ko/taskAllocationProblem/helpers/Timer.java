package cz.dusanjencik.cvut.ko.taskAllocationProblem.helpers;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com
 * created 14.11.15
 *
 * Class for measuring time.
 */
public class Timer {
	private TreeMap<String, Time> timeHashMap = new TreeMap<String, Time>();

	public Timer() {
		timeHashMap.put("_WHOLE", new Time());
	}

	public void start(String name) {
		if (timeHashMap.containsKey(name)) {
			timeHashMap.get(name).start();
		} else
			timeHashMap.put(name, new Time());
	}

	public void init(String name) {
		timeHashMap.put(name, new Time());
	}

	public void elapse(String name) {
		timeHashMap.get(name).elapse();
	}

	public void elapse(String name, long elapsedTime) {
		timeHashMap.get(name).elapse(elapsedTime);
	}

	public void stop(String name) {
		timeHashMap.get(name).stop();
	}

	public void stopAll() {
		timeHashMap.get("_WHOLE").stop();
		System.out.println("\nTIMING:");
		for (Map.Entry<String, Time> entry : timeHashMap.entrySet()) {
			System.out.print(entry.getKey() + " time: " + (entry.getValue().elapsed / 1000000.) + " ms");
			if (entry.getValue().count > 1) {
				System.out.println(" (" + String.format("%.3f", (entry.getValue().elapsed / 1000000. / entry.getValue().count)) + " ms per 1/" + entry.getValue().count + ")");
			} else System.out.println();
		}
	}


	private static class Time {
		long startTime, elapsed;
		int count = 0;

		Time() {
			start();
		}

		void start() {
			startTime = System.nanoTime();
		}

		void elapse() {
			elapsed += System.nanoTime() - startTime;
			++count;
		}

		void elapse(long elapsedTime) {
			elapsed += elapsedTime;
			++count;
		}

		void stop() {
			elapsed = System.nanoTime() - startTime;
			++count;
		}
	}
}
