package cz.dusanjencik.cvut.ko.taskAllocationProblem.helpers;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com 
 * created 05.05.16
 *
 * Helper class for "animating" progress bar. E.g. [###   ] 50 %
 */
public class ProgressBar {
	public static final String TAG = "ProgressBar";

	private final int           width;
	private       StringBuilder stringBuilder;

	public ProgressBar() {
		this(20);
	}

	public ProgressBar(int width) {
		this.width = width;
		stringBuilder = new StringBuilder(width + 2);
		stringBuilder.append("[");
		for (int i = 0; i < width; ++i) {
			stringBuilder.append(' ');
		}
		stringBuilder.append("]");
	}

	public void updateProgress(int percentageProgress) {
		for (int i = 1, sizeOfProgress = (int) (percentageProgress / 100. * width); i <= sizeOfProgress; ++i) {
			stringBuilder.setCharAt(i, '#');
		}
		System.out.printf((char) 0x0d + "%s %d %%", stringBuilder.toString(), percentageProgress);
		if (percentageProgress == 100)
			System.out.printf("\r");
	}
}
