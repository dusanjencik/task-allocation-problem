package cz.dusanjencik.cvut.ko.taskAllocationProblem.io;

import cz.dusanjencik.cvut.ko.taskAllocationProblem.problems.AProblem;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com 
 * created 06.05.16
 *
 * Class for saving and reloading instances of problems.
 */
public class ProblemIO {
	public static final String TAG = "ProblemIO";

	private static final String path     = "./problems";
	private static final String fileName = "problem_";

	/**
	 * Save problem and return its saved path.
	 * @return null if it didn't saved, else the path to saved path.
	 */
	@Nullable
	public static String save(AProblem problem) {
		try {
			Optional<String> maxIntString =
					Files.walk(Paths.get(path))
						 .filter(file -> Files.isRegularFile(file) && file.toString().contains(fileName))
						 .map(path -> path.toString().replaceAll("\\D+", ""))
						 .max(String::compareTo);
			int maxInt = 0;
			if (maxIntString.isPresent())
				maxInt = Integer.parseInt(maxIntString.get());
			++maxInt;
			String             fn  = fileName + maxInt + ".ser";
			String             f   = path + "/" + fn;
			FileOutputStream   fos = new FileOutputStream(f);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(problem);
			oos.close();
			fos.close();
			System.out.println("Successfully saved to " + path + "/" + f);
			return f;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Read a saved instance of problem (AProblem class).
	 */
	@Nullable
	public static AProblem read(String path) {
		try {
			FileInputStream   streamIn          = new FileInputStream(path);
			ObjectInputStream objectinputstream = new ObjectInputStream(streamIn);
			AProblem          problem           = (AProblem) objectinputstream.readObject();
			objectinputstream.close();
			streamIn.close();
			return problem;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
