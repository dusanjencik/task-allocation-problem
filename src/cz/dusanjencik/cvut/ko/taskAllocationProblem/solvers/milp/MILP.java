package cz.dusanjencik.cvut.ko.taskAllocationProblem.solvers.milp;

import cz.dusanjencik.cvut.ko.taskAllocationProblem.helpers.VerboseLevel;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.problems.AProblem;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.solutions.EmptySolution;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.solutions.Solution;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.solvers.ASolver;
import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;


/**
 * @author Dušan Jenčík dusanjencik@gmail.com
 * created 03.05.16
 *
 * Solve problem by Mixed Integer Linear Programming.
 * Complexity of inserting a problem to solver is O(i*j*t^2)
 *
 * MILP:
 * -------------------------------------------------------------------------------------------------------- *
 * minimize sum_i sum_j sum_t F_{i, j} * x_{i, j, t}														*
 * s.t.     sum_j c_{i, j} * x_{i, j, t'} 	<=	C_i			forall_i, forall_t							(a)	*
 * 			sum_i sum_t x_{i, j, t} 		 = 	1			forall_j									(b)	*
 * 			x_{i, j, t}						 =	0			forall_i, forall_j								*
 * 															forall_t: t <= r_j or t > d_j - p_{i, j}	(c)	*
 * 																											*
 * 			t' is element of {t' | t - p_{i, j} <= t < t}
 * 			x_{i, j, t}	= {0, 1}																			*
 * --------------------------------------------------------------------------------------------------------	*
 */
public class MILP extends ASolver {
	public static final String TAG = "MILP";

	private IloCplex cplex;

	public MILP(AProblem problem) {
		super(problem, VerboseLevel.BASIC);
	}

	public MILP(AProblem problem, VerboseLevel verboseLevel) {
		super(problem, verboseLevel);
	}

	@Override
	protected void interrupt() throws IloException {
		IloCplex.Aborter aborter = new IloCplex.Aborter();
		cplex.use(aborter);
		aborter.abort();
	}

	@Override
	protected Solution solveTask() throws IloException {
		cplex = new IloCplex();
		cplex.setParam(IloCplex.IntParam.Threads, 1);
		if (!verboseLevel.atLeast(VerboseLevel.DETAILED)) {
			cplex.setOut(null);
			cplex.setWarning(null);
		}

		// variable: x_{i, j, t} = {0, 1}
		IloNumVar[][][] x = new IloNumVar[problem.getNumOfFacilities()][problem.getNumOfTasks()][problem.getNumOfTimes()];

		/**
		 * criteria function
		 */
		IloLinearNumExpr objective = cplex.linearNumExpr();
		for (int i = 0; i < problem.getNumOfFacilities(); ++i) {
			for (int j = 0; j < problem.getNumOfTasks(); ++j) {
				for (int t = 0; t < problem.getNumOfTimes(); ++t) {
					x[i][j][t] = cplex.intVar(0, 1, "x_{" + i + ", " + j + ", " + t + "}");
					objective.addTerm(problem.getFixedCostPerFacility()[i][j], x[i][j][t]);
				}
			}
		}
		cplex.addMinimize(objective);

		/**
		 * (a)
		 */
		for (int i = 0; i < problem.getNumOfFacilities(); ++i) {
			for (int t = 0; t < problem.getNumOfTimes(); ++t) {
				boolean          addedAtLeastOne = false;
				IloLinearNumExpr sumA            = cplex.linearNumExpr();
				for (int j = 0; j < problem.getNumOfTasks(); ++j) {
					for (int t2 = 0; t2 <= t; ++t2) {
						if (t2 > t - problem.getProcessingTimePerFacility()[i][j]) {
							sumA.addTerm(problem.getCapacityPerFacility()[i][j], x[i][j][t2]);
							addedAtLeastOne = true;
						}
					}
				}
				if (addedAtLeastOne) {
					cplex.addLe(sumA, problem.getFacilityCapacities()[i]);
				}
			}
		}

		/**
		 * (b)
		 */
		for (int j = 0; j < problem.getNumOfTasks(); ++j) {
			IloLinearNumExpr sumB = cplex.linearNumExpr();
			for (int i = 0; i < problem.getNumOfFacilities(); ++i) {
				for (int t = 0; t < problem.getNumOfTimes(); ++t) {
					sumB.addTerm(1, x[i][j][t]);
				}
			}
			cplex.addEq(sumB, 1);
		}

		/**
		 * (c)
		 */
		for (int i = 0; i < problem.getNumOfFacilities(); ++i) {
			for (int j = 0; j < problem.getNumOfTasks(); ++j) {
				for (int t = 0; t < problem.getNumOfTimes(); ++t) {
					if (t < problem.getReleaseTimes()[j] || t > problem.getDeadlines()[j] - problem.getProcessingTimePerFacility()[i][j]) {
						cplex.addEq(x[i][j][t], 0);
					}
				}
			}
		}

		// solve
		boolean  isSolved = cplex.solve();
		Solution solution;
		if (isSolved) {
			boolean[][][] xSolution = new boolean[problem.getNumOfFacilities()][problem.getNumOfTasks()][problem.getNumOfTimes()];
			for (int i = 0; i < problem.getNumOfFacilities(); ++i) {
				for (int j = 0; j < problem.getNumOfTasks(); ++j) {
					for (int t = 0; t < problem.getNumOfTimes(); ++t) {
						if (cplex.getValue(x[i][j][t]) > 0) {
							if (verboseLevel.equals(VerboseLevel.DETAILED))
								System.out.println("x_{" + i + ", " + j + ", " + t + "} = " + (int) cplex.getValue(x[i][j][t]));
						}
						xSolution[i][j][t] = cplex.getValue(x[i][j][t]) > 0;
					}
				}
			}
			solution = new Solution(problem, isSolved, isSolved ? cplex.getObjValue() : 0, xSolution);
		} else solution = new EmptySolution("No feasible solution found.");
		cplex.end();
		return solution;
	}

}
