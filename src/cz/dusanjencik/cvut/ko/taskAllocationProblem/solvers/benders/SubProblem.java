package cz.dusanjencik.cvut.ko.taskAllocationProblem.solvers.benders;

import cz.dusanjencik.cvut.ko.taskAllocationProblem.problems.AProblem;

import java.util.HashSet;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com 
 * created 03.05.16
 *
 * Data class for subProblem instance in Bender's decomposition.
 */
class SubProblem {
	public static final String TAG = "DualProblem";

	AProblem masterProblem;
	private HashSet<Integer> indexesOfTasksTemp = new HashSet<>();
	int[] indexesOfTasks;

	SubProblem(AProblem masterProblem) {
		this.masterProblem = masterProblem;
	}

	void addTask(int j) {
		indexesOfTasksTemp.add(j);
	}

	/**
	 * After packing the access is more flexible to intexesOfTasks array.
	 */
	SubProblem pack() {
		indexesOfTasks = new int[indexesOfTasksTemp.size()];
		int i = 0;
		for (int j : indexesOfTasksTemp)
			indexesOfTasks[i++] = j;
		indexesOfTasksTemp = null;
		return this;
	}
}
