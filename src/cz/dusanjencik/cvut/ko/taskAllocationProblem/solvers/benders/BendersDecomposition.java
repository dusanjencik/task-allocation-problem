package cz.dusanjencik.cvut.ko.taskAllocationProblem.solvers.benders;

import cz.dusanjencik.cvut.ko.taskAllocationProblem.helpers.VerboseLevel;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.problems.AProblem;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.solutions.EmptySolution;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.solutions.Solution;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.solvers.ASolver;
import ilog.concert.*;
import ilog.cplex.IloCplex;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com
 * created 03.05.16
 *
 * Solve masterProblem by Bender's decomposition.
 * Complexity of inserting a masterProblem to solver is 3*i*j.
 * Complexity of inserting a subproblem to solver is 4*j*t.
 *
 * Master masterProblem:
 * ---------------------------------------------------------------------------------------------------- *
 * minimize sum_i sum_j F_{i, j} * x_{i, j}																*
 * s.t.     sum_i x_{i, j} 					 =	1		forall_j									(a)	*
 * 		for infeasible:																					*
 * 			sum_j' (1 - x_{i, j'}			>= 	1		forall_{i from iteration I = 1..H-1}		(b)	*
 * 		for feasible:																					*
 * 			sum_i sum_j F_{i, j} * x_{i, j}	>=	Obj		where Obj is value of objective 				*
 * 														function of subproblem						(c)	*
 * 																										*
 *		relaxation:																						*
 *			sum_j p_{i, j} * c_{i, j} * x_{i, j}  <=  (min_j d_j - max_j r_j) * C_i					(d)	*
 *																										*
 * 			x_{i, j}	= {0, 1}																		*
 * ----------------------------------------------------------------------------------------------------	*
 *
 *
 * Subproblem of masterProblem for fixed i:
 * ---------------------------------------------------------------------------------------------------- *
 * minimize sum_j sum_t F_{i, j} * x_{j, t}																*
 * s.t.     sum_j c_{i, j} * x_{j, t} 	<=	C_i			forall_t									(a)	*
 * 			sum_t x_{j, t} 				 = 	p_{i, j}	forall_j									(b)	*
 * 			x_{j, t}					 =	0			forall_j										*
 * 														forall_t: t < r_j or t > d_j - p_{i, j}		(c)	*
 * 																										*
 * 			x_{j, t}	= {0, 1}																		*
 * ---------------------------------------------------------------------------------------------------- *
 */
public class BendersDecomposition extends ASolver {
	public static final  String TAG              = "BendersDecomposition";
	private static final String TIMER_MASTER     = "Bender's master problem";
	private static final String TIMER_SUBPROBLEM = "Bender's subproblem";

	private IloCplex         cplexMaster;
	private IloCplex         cplexSubProblem;
	private IloNumVar[][][]  x;
	private IloLinearNumExpr objectiveMaster;
	private Solution         finalSolution;

	public BendersDecomposition(AProblem problem) {
		super(problem, VerboseLevel.BASIC);
	}

	public BendersDecomposition(AProblem problem, VerboseLevel verboseLevel) {
		super(problem, verboseLevel);
	}

	@Override
	protected void interrupt() throws IloException {
		IloCplex.Aborter aborter = new IloCplex.Aborter();
		cplexMaster.use(aborter);
		cplexSubProblem.use(aborter);
		aborter.abort();
	}

	@Override
	protected Solution solveTask() throws IloException {
		return solveMasterProblem();
	}

	private Solution solveMasterProblem() throws IloException {
		cplexMaster = new IloCplex();
		cplexMaster.setParam(IloCplex.IntParam.Threads, 1);
		cplexSubProblem = new IloCplex();
		cplexSubProblem.setParam(IloCplex.IntParam.Threads, 1);

		if (!verboseLevel.atLeast(VerboseLevel.DETAILED)) {
			cplexMaster.setOut(null);
			cplexMaster.setWarning(null);
		}

		// variable: x_{i, j} = {0, 1}
		// the last t is used for master problem
		x = new IloNumVar[problem.getNumOfFacilities()][problem.getNumOfTasks()][problem.getNumOfTimes() + 1];

		objectiveMaster = cplexMaster.linearNumExpr();
		for (int j = 0; j < problem.getNumOfTasks(); ++j) {
			IloLinearNumExpr sumA = cplexMaster.linearNumExpr();
			for (int i = 0; i < problem.getNumOfFacilities(); ++i) {
				x[i][j][problem.getNumOfTimes()] = cplexMaster.boolVar("x_{" + i + ", " + j + ", " + problem.getNumOfTimes() + "}");
				/**
				 * criteria function or (c)
				 */
				objectiveMaster.addTerm(problem.getFixedCostPerFacility()[i][j], x[i][j][problem.getNumOfTimes()]);
				/**
				 * (a)
				 */
				sumA.addTerm(1, x[i][j][problem.getNumOfTimes()]);
			}
			cplexMaster.addEq(sumA, 1);
		}
		cplexMaster.addMinimize(objectiveMaster);

		/**
		 * relaxation (d)
		 */
		for (int i = 0; i < problem.getNumOfFacilities(); ++i) {
			IloLinearNumExpr sumD = cplexMaster.linearNumExpr();
			int maxDeadline = Integer.MIN_VALUE,
					minReleaseTime = Integer.MAX_VALUE;
			for (int j = 0; j < problem.getNumOfTasks(); ++j) {
				sumD.addTerm(problem.getProcessingTimePerFacility()[i][j]
									 * problem.getCapacityPerFacility()[i][j]
						, x[i][j][problem.getNumOfTimes()]);
				maxDeadline = Math.max(maxDeadline, problem.getDeadlines()[j]);
				minReleaseTime = Math.min(minReleaseTime, problem.getReleaseTimes()[j]);
			}
			cplexMaster.addLe(sumD, (maxDeadline - minReleaseTime) * problem.getFacilityCapacities()[i]);
		}


		if (verboseLevel.equals(VerboseLevel.BASIC)) {
			System.out.print("#\tmaster\t");
			for (int i = 0; i < problem.getNumOfFacilities(); ++i) {
				System.out.print("sub " + (i + 1) + "\t");
			}
			System.out.println();
		}

		// solve subproblem when the master problem cant find feasible solution.
		cplexMaster.use(new BendersCallback());


		// solve
		if (timer != null) timer.start(TIMER_MASTER);
		boolean isSolved = cplexMaster.solve();
		if (timer != null) timer.elapse(TIMER_MASTER);

		if (verboseLevel.atLeast(VerboseLevel.DETAILED))
			System.out.println("\nObjective value of Master problem = " + cplexMaster.getObjValue());

		cplexMaster.end();
		cplexSubProblem.end();
		if (isSolved && finalSolution != null) return finalSolution;
		return new EmptySolution("No feasible solution found.");
	}

	private void setFinalSolution(Solution[] subProblemSolution, double masterObjective, int iteration) {
		boolean[][][] xSolution = new boolean[problem.getNumOfFacilities()][problem.getNumOfTasks()][problem.getNumOfTimes()];
		for (int i = 0; i < problem.getNumOfFacilities(); ++i) {
			for (int j = 0; j < problem.getNumOfTasks(); ++j) {
				for (int t = 0; t < problem.getNumOfTimes(); ++t) {
					if (subProblemSolution[i].getRawSolution()[i][j][t]) {
						xSolution[i][j][t] = true;
					}
				}
			}
		}
		finalSolution = new Solution(problem, true, masterObjective, xSolution);
		finalSolution.setMessage("Solution found in " + iteration + " iterations.");
	}

	/**
	 * Solving subproblem when the Master problem cant find feasible solution.
	 */
	private class BendersCallback extends IloCplex.LazyConstraintCallback {

		private       int      iteration     = 0;
		private final Solution emptySolution = new EmptySolution("No subproblem solution");


		@Override
		protected void main() throws IloException {
			double masterObjective = getObjValue();

			if (verboseLevel.atLeast(VerboseLevel.DETAILED))
				System.out.println("\n\n\n\n==== solving Master problem in iteration " + iteration + " ====");

			// what tasks are assigned to facility
			SubProblem[] subProblems              = new SubProblem[problem.getNumOfFacilities()];
			double       sumSubProblemsObjectives = 0;
			for (int i = 0; i < problem.getNumOfFacilities(); ++i) {
				SubProblem subProblem = new SubProblem(problem);
				for (int j = 0; j < problem.getNumOfTasks(); ++j) {
					if (getValue(x[i][j][problem.getNumOfTimes()]) > 0) {
						subProblem.addTask(j);
					}
				}
				subProblems[i] = subProblem.pack();
			}


			Solution[]   subProblemSolutions      = new Solution[problem.getNumOfFacilities()];
			boolean      stopSolving              = false;
			for (int i = 0; i < problem.getNumOfFacilities(); ++i) {
				if (verboseLevel.atLeast(VerboseLevel.DETAILED))
					System.out.println("\n==== solving subproblem for fixed i = " + i + " in iteration " + iteration + " ====");
				if (!stopSolving) {
					Solution subSolution = solveSubProblem(subProblems[i], i);
					if (!subSolution.isSolved()) stopSolving = true;
					else subProblemSolutions[i] = subSolution;
				}
				if (subProblemSolutions[i] != null && subProblemSolutions[i].isSolved()) {
					/**
					 * (c)
					 */
					add(cplexMaster.ge(objectiveMaster, subProblemSolutions[i].getObjectiveValue()));
					if (verboseLevel.atLeast(VerboseLevel.DETAILED))
						System.out.println("Objective value of subproblem = " + subProblemSolutions[i].getObjectiveValue());
					sumSubProblemsObjectives += subProblemSolutions[i].getObjectiveValue();
				} else {
					/**
					 * (b)
					 */
					IloLinearNumExpr sumB  = cplexMaster.linearNumExpr();
					int              count = 0;
					boolean          any   = false;
					for (int j : subProblems[i].indexesOfTasks) {
						sumB.addTerm(-1, x[i][j][problem.getNumOfTimes()]);
						++count;
						any = true;
					}
					if (any)
						add(cplexMaster.ge(sumB, 1 - count));
				}

			}
			if (verboseLevel.equals(VerboseLevel.BASIC)) {
				System.out.print(iteration + ".   " + (iteration < 10 ? " " : "") + masterObjective + "\t ");
				for (int i = 0; i < problem.getNumOfFacilities(); ++i) {
					if (subProblemSolutions[i] != null)
						System.out.print(subProblemSolutions[i].getObjectiveValue() + "\t ");
				}
				System.out.println();
			}

			// is optimum?
			if (Math.abs(masterObjective - sumSubProblemsObjectives) < 1) {
				setFinalSolution(subProblemSolutions, masterObjective, iteration);
			}
			++iteration;
		}

		private Solution solveSubProblem(SubProblem problem, int i) throws IloException {
			cplexSubProblem.clearModel();

			if (!verboseLevel.atLeast(VerboseLevel.DETAILED)) {
				cplexSubProblem.setOut(null);
				cplexSubProblem.setWarning(null);
			}

			/**
			 * criteria function
			 */
			IloLinearNumExpr objective = cplexSubProblem.linearNumExpr();
			for (int j : problem.indexesOfTasks) {
				for (int t = 0; t < problem.masterProblem.getNumOfTimes(); ++t) {
					x[i][j][t] = cplexMaster.boolVar("x_{" + i + ", " + j + ", " + t + "}");
					objective.addTerm(problem.masterProblem.getFixedCostPerFacility()[i][j], x[i][j][t]);
				}
			}
			cplexSubProblem.addMinimize(objective);

			/**
			 * (a)
			 */
			for (int t = 0; t < problem.masterProblem.getNumOfTimes(); ++t) {
				boolean          addedAtLeastOne = false;
				IloLinearNumExpr sumA            = cplexSubProblem.linearNumExpr();
				for (int j : problem.indexesOfTasks) {
					for (int t2 = 0; t2 <= t; ++t2) {
						if (t2 > t - problem.masterProblem.getProcessingTimePerFacility()[i][j]) {
							sumA.addTerm(problem.masterProblem.getCapacityPerFacility()[i][j], x[i][j][t2]);
							addedAtLeastOne = true;
						}
					}
				}
				if (addedAtLeastOne)
					cplexSubProblem.addLe(sumA, problem.masterProblem.getFacilityCapacities()[i]);
			}

			/**
			 * (b)
			 */
			for (int j : problem.indexesOfTasks) {
				IloLinearNumExpr sumB = cplexSubProblem.linearNumExpr();
				for (int t = 0; t < problem.masterProblem.getNumOfTimes(); ++t) {
					sumB.addTerm(1, x[i][j][t]);
				}
				cplexSubProblem.addEq(sumB, 1);
			}

			/**
			 * (c)
			 */
			for (int j : problem.indexesOfTasks) {
				for (int t = 0; t < problem.masterProblem.getNumOfTimes(); ++t) {
					if (t < problem.masterProblem.getReleaseTimes()[j] || t > problem.masterProblem.getDeadlines()[j] - problem.masterProblem.getProcessingTimePerFacility()[i][j]) {
						cplexSubProblem.addEq(x[i][j][t], 0);
					}
				}
			}


			// solve
			if (timer != null) timer.start(TIMER_SUBPROBLEM);
			boolean isSolved = cplexSubProblem.solve();
			if (timer != null) timer.elapse(TIMER_SUBPROBLEM);

			Solution solution;
			if (isSolved) {
				boolean[][][] xSolution = new boolean[problem.masterProblem.getNumOfFacilities()][problem.masterProblem.getNumOfTasks()][problem.masterProblem.getNumOfTimes()];
				for (int j : problem.indexesOfTasks) {
					for (int t = 0; t < problem.masterProblem.getNumOfTimes(); ++t) {
						if (cplexSubProblem.getValue(x[i][j][t]) > 0) {
							if (verboseLevel.atLeast(VerboseLevel.DETAILED))
								System.out.println("x_{" + i + ", " + j + ", " + t + "} = " + (int) cplexSubProblem.getValue(x[i][j][t]));
							xSolution[i][j][t] = true;
						}
					}
				}
				solution = new Solution(problem.masterProblem, true, cplexSubProblem.getObjValue(), xSolution);
			} else {
				solution = emptySolution;
			}
			return solution;
		}

	}
}
