package cz.dusanjencik.cvut.ko.taskAllocationProblem.solvers;

import cz.dusanjencik.cvut.ko.taskAllocationProblem.helpers.Outer;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.helpers.Timer;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.helpers.VerboseLevel;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.problems.AProblem;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.solutions.Solution;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.solutions.TimeoutSolution;
import ilog.concert.IloException;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 03.05.16
 *
 * Abstract data class for solvers.
 */
public abstract class ASolver {
	public static final String TAG = "ASolution";

	protected static final int MAX_TIME_LIMIT = 900; // 15 min

	protected AProblem     problem;
	protected VerboseLevel verboseLevel;
	protected Timer        timer;
	private int timeLimit = MAX_TIME_LIMIT;

	public ASolver(AProblem problem, VerboseLevel verboseLevel) {
		this.problem = problem;
		this.verboseLevel = verboseLevel;
	}

	protected abstract void interrupt() throws IloException;

	public void setTimer(Timer timer) {
		this.timer = timer;
	}

	public ASolver setTimeLimit(int timeLimit) {
		this.timeLimit = timeLimit;
		return this;
	}

	public Solution solve() throws IloException {
		final Outer<Solution> result = new Outer<>(null);
		try {
			new TimeLimit(timeLimit, new ExceptionRunnable() {
				@Override
				public void run() throws Exception {
					result.set(solveTask());
				}

				@Override
				public void onTimeout() throws Exception {
					interrupt();
					result.set(new TimeoutSolution());
				}
			}).run();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result.get();
	}

	protected abstract Solution solveTask() throws IloException;

	interface ExceptionRunnable {
		void run() throws Exception;

		void onTimeout() throws Exception;
	}

	public class TimeLimit implements ExceptionRunnable {
		private final ExceptionRunnable r;
		private final int               limit;
		private       Throwable         t;

		public TimeLimit(final int limit, final ExceptionRunnable r) {
			this.limit = limit;
			this.r = r;
		}

		public synchronized void run() throws Exception {
			final Thread thread = new Thread(() -> {
				try {
					r.run();
				} catch (final Throwable t1) {
					TimeLimit.this.t = t1;
				}
			});
			thread.start();
			try {
				thread.join(limit * 1000);
				if (thread.isAlive()) {
					thread.interrupt();
					r.onTimeout();
				}
			} catch (final InterruptedException e) {
				if (t == null) {
					t = e;
				}
			}
			if (t != null) {
				t = null;
			}
		}

		@Override
		public void onTimeout() {
			// not implemented
		}
	}
}
