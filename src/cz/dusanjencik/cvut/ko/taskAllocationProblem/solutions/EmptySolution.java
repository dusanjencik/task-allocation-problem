package cz.dusanjencik.cvut.ko.taskAllocationProblem.solutions;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com 
 * created 03.05.16
 *
 * Empty data class for error solution
 */
public class EmptySolution extends Solution {
	public static final String TAG = "EmptySolution";

	private String errorMessage;

	public EmptySolution(String errorMessage) {
		super(null, false, 0, null);
		this.errorMessage = errorMessage;
	}

	@Override
	public void printSolution() {
		super.printSolution(errorMessage);
	}
}
