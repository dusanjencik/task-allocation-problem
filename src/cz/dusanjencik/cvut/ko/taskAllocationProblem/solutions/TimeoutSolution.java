package cz.dusanjencik.cvut.ko.taskAllocationProblem.solutions;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com 
 * created 07.05.16
 */
public class TimeoutSolution extends EmptySolution {
	public static final String TAG = "TimeoutSolution";

	public TimeoutSolution() {
		super("Timeout");
	}

	@Override
	public void printSolution(String message, TypeOfPrint typeOfPrint) {
		System.err.println("Timeout");
	}
}
