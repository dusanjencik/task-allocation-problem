package cz.dusanjencik.cvut.ko.taskAllocationProblem.solutions;

import cz.dusanjencik.cvut.ko.taskAllocationProblem.problems.AProblem;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 01.05.16
 *
 * Data class for any solution.
 */
public class Solution {
	public static final String TAG = "Solution";

	public enum TypeOfPrint {TASK_ASSIGNMENT_TO_FACILITY, FACILITY_CONTAINS_TASKS}

	private final AProblem      problem;
	private final double        objectiveValue;
	private final boolean[][][] x;
	private final boolean       isSolved;
	protected     String        messageToPrint;

	public Solution(AProblem problem, boolean isSolved, double objectiveValue, boolean[][][] x) {
		this.problem = problem;
		this.objectiveValue = objectiveValue;
		this.x = x;
		this.isSolved = isSolved;
	}

	public boolean isSolved() {
		return isSolved;
	}

	public boolean[][][] getRawSolution() {
		return x;
	}

	public Solution setMessage(String messageToPrint) {
		this.messageToPrint = messageToPrint;
		return this;
	}

	public double getObjectiveValue() {
		return objectiveValue;
	}

	public void printSolution() {
		printSolution(null);
	}

	public void printSolution(String message) {
		printSolution(message, TypeOfPrint.FACILITY_CONTAINS_TASKS);
	}

	public void printSolution(String message, TypeOfPrint typeOfPrint) {
		if (!isSolved) {
			if (message != null && !message.isEmpty()) {
				System.err.println("There was a problem.");
				System.err.println(message);
			} else
				System.err.println("There is no optimal solution.");
			return;
		}
		System.out.println("\n---------------------\nThe final cost is " + objectiveValue);
		if (message != null && !message.isEmpty())
			System.out.println(message);
		if (messageToPrint != null && !messageToPrint.isEmpty())
			System.out.println(messageToPrint);
		if (typeOfPrint == TypeOfPrint.TASK_ASSIGNMENT_TO_FACILITY) {
			System.out.println("\nTask assignment:");
			for (int j = 0; j < problem.getNumOfTasks(); ++j) {
				System.out.println("T" + (j + 1) + ":");
				for (int i = 0; i < problem.getNumOfFacilities(); ++i) {
					System.out.print("\tFacility " + (i + 1) + ": [ ");
					StringBuilder sb = new StringBuilder("");
					for (int t = 0; t < problem.getNumOfTimes(); ++t) {
						if (x[i][j][t]) {
							for (int p = 0; p < problem.getProcessingTimePerFacility()[i][j]; ++p) {
								sb.append(t + p + 1).append(", ");
							}
							break;
						}
					}
					if (sb.length() > 2)
						sb.deleteCharAt(sb.length() - 2);
					System.out.println(sb.toString() + "]");
				}
			}
		} else {
			System.out.println("Facility assignment:");
			for (int i = 0; i < problem.getNumOfFacilities(); ++i) {
				System.out.println("Facility " + (i + 1) + ":");
				for (int j = 0; j < problem.getNumOfTasks(); ++j) {
					System.out.print("\tT" + (j + 1) + ": [ ");
					StringBuilder sb = new StringBuilder("");
					for (int t = 0; t < problem.getNumOfTimes(); ++t) {
						if (x[i][j][t]) {
							for (int p = 0; p < problem.getProcessingTimePerFacility()[i][j]; ++p) {
								sb.append(t + p + 1).append(", ");
							}
							break;
						}
					}
					if (sb.length() > 2)
						sb.deleteCharAt(sb.length() - 2);
					System.out.println(sb.toString() + "]");
				}

				// check capacity overloading
				for (int t = 0; t < problem.getNumOfTimes(); ++t) {
					double cummulateC = 0;
					for (int j = 0; j < problem.getNumOfTasks(); ++j) {
						if (x[i][j][t]) cummulateC += problem.getCapacityPerFacility()[i][j];
					}
					if (cummulateC > problem.getFacilityCapacities()[i]) {
						throw new RuntimeException("Facility " + (i + 1) + " capacity overloaded! (" + cummulateC + " > " + problem.getFacilityCapacities()[i] + ")");
					}
				}

			}
		}
	}
}
