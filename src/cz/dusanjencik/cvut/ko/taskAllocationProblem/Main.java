package cz.dusanjencik.cvut.ko.taskAllocationProblem;

import cz.dusanjencik.cvut.ko.taskAllocationProblem.helpers.Timer;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.helpers.VerboseLevel;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.io.ProblemIO;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.problems.*;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.solutions.EmptySolution;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.solutions.Solution;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.solvers.ASolver;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.solvers.benders.BendersDecomposition;
import cz.dusanjencik.cvut.ko.taskAllocationProblem.solvers.milp.MILP;
import ilog.concert.IloException;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 03.05.16
 */
public class Main {

	private static final String TIMER_BENDER = "Bender's decomposition";
	private static final String TIMER_MILP   = "MILP";

	public static void main(String[] args) throws IloException {
		Timer timer = new Timer();

//		AProblem problem = ProblemIO.read("./problems/problem_4.ser");
		AProblem problem = new Problem_2facilities_8tasks();
//		AProblem problem = new ProblemGenerator(3, 50).generateProblem();

		VerboseLevel verboseLevel = VerboseLevel.BASIC;

		System.out.println("\n\n\n\n\n=============================" +
								   "\n	   SOLVING BY MILP    " +
								   "\n=============================");
		timer.start(TIMER_MILP);
		ASolver  milp         = new MILP(problem, verboseLevel);
		Solution milpSolution = milp.solve();
		milpSolution.printSolution(null, Solution.TypeOfPrint.FACILITY_CONTAINS_TASKS);
		timer.stop(TIMER_MILP);


		System.out.println("\n\n\n\n===================================" +
								   "\n SOLVING BY BENDER'S DECOMPOSITION " +
								   "\n===================================");
		timer.start(TIMER_BENDER);
		ASolver benders = new BendersDecomposition(problem, verboseLevel);
		benders.setTimer(timer);
		benders.solve().printSolution(null, Solution.TypeOfPrint.FACILITY_CONTAINS_TASKS);
		timer.stop(TIMER_BENDER);


		timer.stopAll();
	}
}
