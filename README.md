# Task allocation problem #

Semestrální projekt na předmět Kombinatorická optimalizace.

Tato práce ([report](report.pdf)) se zabývá analýzou a později návrhem řešení a implementací pro problém rozvrhování úkolů pro různá zařízení s omezenou kapacitou. Problém je formulován pomocí MILP (Mixed Integer Linear Programming) a LBBD (Logic Based Bender's decomposition). Oba přístupy jsou porovnány a následně je provedeno několik měření časů výpočtu. Implementace je pomocí solveru CPLEX.